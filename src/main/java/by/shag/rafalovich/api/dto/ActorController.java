package by.shag.rafalovich.api.dto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ActorController {


    @DeleteMapping("actors")
    public String delete(@RequestParam("id")Integer id) {
        return service.delete(id);
    }


    @DeleteMapping("actors")
    public String delete(Model model) {
        Long id = (Long) model.getAttribute("id");
        return service.delete(id);
    }
}
