package by.shag.rafalovich.api;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="id", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/greeting0")
    public String greeting0(
            @RequestParam(name="name") String name,
            @RequestParam(name="lastname") String lastname,
            @RequestParam(name="age") Integer age
    ) {

        return "helloworld.html";
    }

    @GetMapping("/greeting2")
    public String greeting2() {
        return "helloworld.html";
    }

    @GetMapping("/home")
    public String home() {
        return "home.html";
    }

    @GetMapping("/xml")
    public String xml() {
        return "1.xml";
    }
}