package by.shag.rafalovich.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import by.shag.rafalovich.api.dto.StudentDto;
import org.springframework.stereotype.Service;

@Service
public class StudentService {


    private static int ID_SEQUENCE = 5;

    private List<StudentDto> studentsFromDb;

    public StudentService() {
        this.studentsFromDb = new ArrayList<>();
        studentsFromDb.add(new StudentDto(1, "Dima", "Rafalovich", 1992));
        studentsFromDb.add(new StudentDto(2, "Dima", "Danilovich", 1995));
        studentsFromDb.add(new StudentDto(3, "Dima", "Manakov", 1993));
        studentsFromDb.add(new StudentDto(4, "Dima", "Litvinov", 1982));
    }


    public StudentDto create(StudentDto dto) {
        s;jpo


        dto.setId(ID_SEQUENCE ++);
        studentsFromDb.add(dto);
        return dto;

        nknlkjp
    }

    public List<StudentDto> findAll() {
        return studentsFromDb;
    }

    public StudentDto findById(Integer id) {
        Optional<StudentDto> desiredStudent = studentsFromDb.stream()
                .filter(student -> id.equals(student.getId()))
                .findFirst();

        return desiredStudent.orElseThrow(() -> new RuntimeException("Not found"));
    }

    public StudentDto update(StudentDto studentDto) {
        StudentDto persistedStudent = findById(studentDto.getId());

        persistedStudent.setName(studentDto.getName());
        persistedStudent.setLastName(studentDto.getLastName());
        persistedStudent.setYearOfBirth(studentDto.getYearOfBirth());
        return persistedStudent;
    }


    public void delete(Integer id) {
        StudentDto persistedStudent = findById(id);
        studentsFromDb.remove(persistedStudent);
    }
}
